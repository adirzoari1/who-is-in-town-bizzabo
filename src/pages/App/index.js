import React from 'react'
import { Col } from "antd";
import { useSelector } from "react-redux";
import { StyledRow } from './style'
import { AppLayout, EventDetails, FavoritesEventsList, SearchResultSection } from '../../components'
import { selectedEventSelector } from '../../store/artist/artist.selectors';


function App() {
  const selectedEvent = useSelector(selectedEventSelector);

  return (
    <AppLayout>
      <StyledRow justify="space-around" >
        <Col span={8}>
          <SearchResultSection />
        </Col>
        <Col span={10}  >
          {selectedEvent && <EventDetails />}
        </Col>
        <Col span={4} >
          <FavoritesEventsList />
        </Col>
      </StyledRow>
    </AppLayout>
  )
}

export default App
