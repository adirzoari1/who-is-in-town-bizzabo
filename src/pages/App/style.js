import styled from "styled-components";
import { Row } from "antd";

const StyledRow = styled(Row)`
   width:90%;
`;


export {
    StyledRow,
}