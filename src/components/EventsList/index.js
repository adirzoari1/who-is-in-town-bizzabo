import React from 'react'
import { List } from 'antd';
import { useSelector, useDispatch } from "react-redux";
import { artistEventsSelector } from '../../store/artist/artist.selectors'
import { EventItem } from '..';
import { selectEventArtist } from '../../store/artist/artist.actions';

function EventsList() {
    const artistEvents = useSelector(artistEventsSelector);
    const dispatch = useDispatch()

    function onSelectEvent(event) {
        dispatch(selectEventArtist(event))
    }
 
    return (
        <List
            itemLayout="vertical"
            header={<div>Upcoming Events ({artistEvents.length})</div>}
            dataSource={artistEvents}
            renderItem={(item) => <EventItem
                {...item}
                showFavoriteButton ={false}
                onSelectedEvent={() => { onSelectEvent(item) }}
            />}
        />
    )
}

export default EventsList
