import styled from "styled-components";
import { Layout } from "antd";
const { Header, Content, Footer } = Layout;

const StyledLayout = styled(Layout)`
    min-height:100vh;
`;
const StyledHeader = styled(Header)`
    display:flex;
    justify-content:center;
    align-items: center;
    background:linear-gradient(270deg,#39c 0%,#4ec3c8 99.54%,#4ec3c8 100%)
   
`;
const StyledContent = styled(Content)`
    padding: 20px 50px;
    display: flex;
    justifyContent: center;
    background:#fff;
    height:500px;
    overflow-y:auto;
`;

const StyledFooter = styled(Footer)`
    text-align: center;
`




export {
    StyledLayout,
    StyledHeader,
    StyledContent,
    StyledFooter
}