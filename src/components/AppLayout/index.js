import React from 'react'
import { Typography } from 'antd';

import {
    StyledLayout,
    StyledHeader,
    StyledContent,
    StyledFooter
} from './style'

const { Title } = Typography;



function AppLayout({ children }) {

    return (
        <StyledLayout>
            <StyledHeader> 
                <Title strong level={4}>
                    Who's In Town App
                </Title>
            </StyledHeader>
            <StyledContent>
                {children}
            </StyledContent>
            <StyledFooter>Bizzabo assignment By Adir Zoari</StyledFooter>
        </StyledLayout>
    )
}

export default AppLayout
