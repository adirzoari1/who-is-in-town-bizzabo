import React from 'react'
import { List, Card, Button } from 'antd';
import { HeartFilled, DragOutlined } from '@ant-design/icons';
function EventItem({ id, onSelectedEvent, onToggleFavoriteEvent, showFavoriteButton }) {
 
    return (
        <List.Item key={id}>
            <Card
                size="small"
                hoverable

                actions={[<Button icon={<DragOutlined />} onClick={onSelectedEvent}>Show Event</Button>,showFavoriteButton&&<Button shape="circle" icon={<HeartFilled />} onClick={onToggleFavoriteEvent} />]}
                style={{ width: 300 }}

            >
                Event id:{id}

            </Card>

        </List.Item>

    )
}

export default EventItem
