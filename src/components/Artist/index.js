import React from 'react'
import { Image, Space, Typography,Descriptions } from 'antd';
import { useSelector } from "react-redux";
import { artistDetailsSelector } from '../../store/artist/artist.selectors'

const { Title } = Typography
function Artist() {
    const aristDetails = useSelector(artistDetailsSelector);
    return (
        <Space>
            <Image
                alt="artist_image"
                width={150}
                src={aristDetails?.image_url}

            />
            <Descriptions title="Artist Details">
            <Descriptions.Item label="Name:">{aristDetails?.name}</Descriptions.Item>
                </Descriptions>
        </Space>
    )
}

export default Artist
