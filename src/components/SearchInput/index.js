import React, { useEffect, useState } from 'react'
import { Button } from 'antd'
import { RestOutlined } from '@ant-design/icons';
import { StyledSpace, StyledSearchInput } from './style'

function SearchInput({ onSearch, onResetSearch, historyArtistTerm }) {
    const [searchTerm, setSearchTerm] = useState('')


    useEffect(() => {
        setSearchTerm(historyArtistTerm)

    }, [historyArtistTerm]);
    function onPressSearch() {
        if (searchTerm) {
            onSearch(searchTerm)
        } else {
            onResetSearch()
        }
    }

    return (
        <StyledSpace align="center">
            <StyledSearchInput
                placeholder="Search artist..."
                onSearch={onPressSearch}
                onPressEnter={onPressSearch}
                onChange={(e) => setSearchTerm(e.target.value)}
                value={searchTerm}
                enterButton
            />
            <Button icon={<RestOutlined />} onClick={onResetSearch} />


        </StyledSpace>
    )
}

export default SearchInput
