import React from 'react'
import { Tag, Typography } from 'antd'
import { useSelector } from "react-redux";
import { artistsHistorySearchSelector } from '../../store/artist/artist.selectors';
import { StyledPopover,WrapperHistoryTags } from './style'
const { Text } = Typography;

function ArtistHistorySearch({ onClick }) {
    const artistHistorySearchList = useSelector(artistsHistorySearchSelector);
    const content = (
        <WrapperHistoryTags size={[10, 16]} wrap>
            {artistHistorySearchList.map(artist => <Tag color="#4ec3c8" onClick={() => { onClick(artist) }}>{artist}</Tag>)}
        </WrapperHistoryTags>
    );
    return (
        <StyledPopover placement="topRight" title={`History Search (${artistHistorySearchList.length})`} content={content} trigger="click">
            <Text type="secondary">Show History Search</Text>
        </StyledPopover>
    )
}

export default ArtistHistorySearch
