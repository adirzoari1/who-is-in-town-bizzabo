import styled from "styled-components";
import { Popover,Space } from "antd";

const StyledPopover = styled(Popover)`
    display:block;
    margin-bottom:30px;
`

const WrapperHistoryTags =  styled(Space)`
    width:150px;
`


export {
    StyledPopover,
    WrapperHistoryTags
}