import React from 'react'
import { List } from 'antd';
import { useSelector, useDispatch } from "react-redux";
import { favoritesEventsSelector } from '../../store/favoriteEvent/favoriteEvent.selectors';
import { EventItem } from '..';
import { toggleFavoriteEvent } from '../../store/favoriteEvent/favoriteEvent.actions';
import { selectEventArtist } from '../../store/artist/artist.actions';

function FavroitesEventsList() {
    const favoritesEvents = useSelector(favoritesEventsSelector);
    const dispatch = useDispatch()

    function onToggleFavoriteEvent(event) {
        dispatch(toggleFavoriteEvent(event))
    }
    function onSelectEvent(event) {
        dispatch(selectEventArtist(event))
    }
    return (
        <List
            itemLayout="vertical"
            header={<div>My Favorites Events ({favoritesEvents.length})</div>}
            dataSource={favoritesEvents}
            renderItem={(item) => <EventItem
                {...item}
                showFavoriteButton={true}
                onSelectedEvent={() => { onSelectEvent(item) }}
                onToggleFavoriteEvent={() => { onToggleFavoriteEvent(item) }}
            />}
        />
    )
}

export default FavroitesEventsList
