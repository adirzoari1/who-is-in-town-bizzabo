import React from 'react'
import { Descriptions, List, Typography, Tag, Button, Space } from 'antd';
import { HeartFilled, HeartOutlined } from '@ant-design/icons';
import moment from 'moment'
import { useSelector, useDispatch } from "react-redux";
import { selectedEventSelector } from '../../store/artist/artist.selectors'
import { toggleFavoriteEvent } from '../../store/favoriteEvent/favoriteEvent.actions';
const { Text, Link } = Typography;


function EventDetails() {
  const eventDetails = useSelector(selectedEventSelector);
  const dispatch = useDispatch();
  const DescriptionTitle = () => (
    <Space>
      <Text>Event Details</Text>
      <Button shape="circle" onClick={() => { dispatch(toggleFavoriteEvent(eventDetails)) }} icon={eventDetails.isFavorite ? <HeartFilled /> : <HeartOutlined />} />
    </Space>
  )
  return (
    <Descriptions title={<DescriptionTitle />} layout="vertical" size="large" bordered>
      <Descriptions.Item label="Id">{eventDetails.id}</Descriptions.Item>
      <Descriptions.Item label="Event Date">{moment(eventDetails.datetime).format('DD-MM-YYYY')}</Descriptions.Item>
      <Descriptions.Item label="Venu">
        Country: {eventDetails.venue?.country}
        <br />
          City: {eventDetails.venue?.city}
        <br />
          Name: {eventDetails.venue?.name}
        <br />
          Region: {eventDetails.venue?.region}
      </Descriptions.Item>
      <Descriptions.Item label="Offers">
        <List
          bordered
          dataSource={eventDetails.offers}
          renderItem={item => (
            <List.Item
              actions={[<Link href={item.url} target="_blank">Buy Tickets</Link>]}
            >
              <Text>Type: {item.type}</Text>
              <Text>Status: <Tag color="green">{item.status}</Tag></Text>

            </List.Item>
          )}
        />
      </Descriptions.Item>
    </Descriptions>
  )
}

export default EventDetails;
