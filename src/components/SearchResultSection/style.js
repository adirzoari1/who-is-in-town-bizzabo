import styled from "styled-components";
import { Input,Space } from "antd";
const { Search } = Input;

const StyledSpace = styled(Space)`
    margin-bottom:20px;
`
const StyledSearchInput = styled(Search)`
    width:250px;
    display:block;
`;





export {
    StyledSpace,
    StyledSearchInput
}