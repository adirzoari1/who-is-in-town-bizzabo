import React, { useState } from 'react'
import { Spin, Typography } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { SearchInput, ArtistHistorySearch, Artist, EventsList } from '..';
import { resetSearch, searchArtistRequest } from '../../store/artist/artist.actions';
import { errorMsgSelector, isLoadingSelector, artistDetailsSelector } from '../../store/artist/artist.selectors';

const { Text } = Typography


function SearchResultSection() {
    const [historyArtistTerm, setHistoryArtistTerm] = useState(null)
    const dispatch = useDispatch();
    const errorMsg = useSelector(errorMsgSelector);
    const isLoading = useSelector(isLoadingSelector);
    const artistDetails = useSelector(artistDetailsSelector);


    function onSearchArtist(arist) {
        dispatch(searchArtistRequest(arist))
    }
    function onResetSearch() {
        dispatch(resetSearch())
        setHistoryArtistTerm(null)

    }

    function onClickArtistHistory(artist) {
        setHistoryArtistTerm(artist)
        onSearchArtist(artist)
    }

    const DisplayEventSection = () =>
        <div>
            {isLoading && !errorMsg ?
                <Spin title="loading..." />
                : errorMsg ? <Text type="danger">{errorMsg}</Text>
                    : artistDetails && <> <Artist />
                        <EventsList /></>}
        </div>


    return (
        <>
            <SearchInput
                onSearch={onSearchArtist} onResetSearch={onResetSearch}
                historyArtistTerm={historyArtistTerm}
            />
            <ArtistHistorySearch onClick={onClickArtistHistory} />
            <DisplayEventSection />
        </>

    )


}

export default SearchResultSection
