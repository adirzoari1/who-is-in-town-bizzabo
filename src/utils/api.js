import axios from 'axios';

const APP_ID = 123
const API_URLS = {
    GET_ARTIST_INFORMATION: (artistName) => `https://rest.bandsintown.com/artists/${artistName}/?app_id=${APP_ID}`,
    GET_ARTIST_EVENTS: (artistName) => `https://rest.bandsintown.com/artists/${artistName}/events?app_id=${APP_ID}`,
}


const getArtistInformation = (artistName) => {
    return axios.get(API_URLS.GET_ARTIST_INFORMATION(artistName))
}

const getArtistEvents = (artistName) => {
    return axios.get(API_URLS.GET_ARTIST_EVENTS(artistName))
}


export {
    getArtistInformation,
    getArtistEvents

}