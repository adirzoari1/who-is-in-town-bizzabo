import { filter, some } from 'lodash'
import Types from './favoriteEvent.types'
const initialState = {
    favoritesEvents: []
};

const favoriteEventReducer = (state = initialState, { type, payload }) => {

    switch (type) {
        case Types.TOGGLE_FAVORITE_EVENT: {
            const { event } = payload
            if (some(state.favoritesEvents, fe => fe.id === event.id)) {
                return {
                    ...state,
                    favoritesEvents: filter(state.favoritesEvents, fe => fe.id !== event.id)
                }
            }
            return { ...state, favoritesEvents: [...state.favoritesEvents,{...event}] }
        }

        default:
            return state;
    }
};

export default favoriteEventReducer;
