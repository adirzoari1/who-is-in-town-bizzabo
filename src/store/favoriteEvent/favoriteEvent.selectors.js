import { createSelector } from 'reselect';
import {get} from 'lodash'

const baseFavoriteEvent = state=>state.favoriteEvent

const favoritesEventsSelector = createSelector(baseFavoriteEvent,baseFavoriteEvent=>get(baseFavoriteEvent,'favoritesEvents',[]))


export {
    favoritesEventsSelector,
}
