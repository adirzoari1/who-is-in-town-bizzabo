import Types from './favoriteEvent.types'

const toggleFavoriteEvent = event => ({
    type: Types.TOGGLE_FAVORITE_EVENT,
    payload: { event}
});

export {
    toggleFavoriteEvent
}