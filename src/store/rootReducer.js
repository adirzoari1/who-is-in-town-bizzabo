import { combineReducers } from 'redux';

import artistReducer from './artist/artist.reducer';
import favoriteEventReducer from './favoriteEvent/favoriteEvent.reducer';

const rootReducer = combineReducers({
    artist: artistReducer,
    favoriteEvent: favoriteEventReducer
});

export default rootReducer;


