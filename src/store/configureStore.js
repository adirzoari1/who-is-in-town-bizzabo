import { createStore, applyMiddleware } from 'redux';
import { persistStore } from 'redux-persist'; // Importing redux persist to config persistence for the store
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createFilter } from 'redux-persist-transform-filter';


import createSagaMiddleware from 'redux-saga';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const subsetArtistReducer = createFilter('artist', ['artistHistorySearch']);

export const persistConfig = {
    key: 'root',
    storage,
    whitelist:['favoriteEvent','artist'],
    transforms: [subsetArtistReducer]
};



const sagaMiddleware = createSagaMiddleware();
const persistedReducer = persistReducer(persistConfig, rootReducer);

const middlewares = [sagaMiddleware];
const store = createStore(persistedReducer, applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);


const persistor = persistStore(store);
export {
    store, persistor
}


