import { takeLatest, put, all, call } from 'redux-saga/effects';
import {get} from 'lodash'
import Types from './artist.types'
import { searchArtistSuccess, searchArtistFailure } from '../artist/artist.actions';
import { getArtistEvents, getArtistInformation } from '../../utils/api';



function* onSearchArtistAsync(action) {
    try {
        const { payload } = action
        let response = yield call(getArtistInformation, payload)
        if(!response.data || response?.data?.error){
            yield put(searchArtistFailure('Artist not found'));
            return;

        }
        if (response?.data?.upcoming_event_count > 0) {
            const eventResponse = yield call(getArtistEvents, payload)
            response.events = get(eventResponse,'data',[])
        }
        const responseData = {
            artist:get(response,'data',null),
            events:get(response,'events',[])
        }
        yield put(searchArtistSuccess({...responseData}));
    } catch (error) {
        yield put(searchArtistFailure(error));
    }
}

function* artistSaga() {
    yield all([
        takeLatest(Types.SEARCH_ARTIST_REQUEST, onSearchArtistAsync)

    ])
}

export default artistSaga;