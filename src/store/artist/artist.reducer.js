import Types from './artist.types'

const initialState = {
    artist: null,
    events: [],
    selectedEvent: null,
    isLoading: false,
    errorMsg: null,
    artistHistorySearch: [],
};

const resetState = {
    artist: null,
    events: [],
    selectedEvent: null,
    errorMsg: null
}
const artistReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case Types.SEARCH_ARTIST_REQUEST:
            const artistHistoryList = state.artistHistorySearch;
             if(!artistHistoryList.includes(payload)){
                artistHistoryList.push(payload)
            }
            return {
                ...state,
                ...resetState,
                artistHistorySearch:[...artistHistoryList],
                isLoading: true,

            };
        case Types.SEARCH_ARTIST_SUCCESS:
            const { artist, events } = payload
            return {
                ...state,
                artist,
                events,
                isLoading: false,

            }
        case Types.SEARCH_ARTIST_FAILURE:

            return {
                ...state,
                errorMsg: payload,
                isLoading: false,

            }
        case Types.RESET_SEARCH: {
            return {
                ...state,
                ...resetState
            }
        }
        case Types.SELECT_EVENT_ARTIST: {
            return {
                ...state,
                selectedEvent: payload,

            }
        }

        default:
            return state;
    }
};

export default artistReducer;
