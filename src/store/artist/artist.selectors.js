import { createSelector } from 'reselect';
import { get, some } from 'lodash'
import { favoritesEventsSelector } from '../favoriteEvent/favoriteEvent.selectors'

const baseArtistSelector = state => state.artist
const artistDetailsSelector = createSelector(baseArtistSelector, baseArtist => get(baseArtist, 'artist', null))
const artistEventsSelector = createSelector(baseArtistSelector, baseArtist => get(baseArtist, 'events', []))
const artistsHistorySearchSelector = createSelector(baseArtistSelector, baseArtist => get(baseArtist, 'artistHistorySearch', []))
const isLoadingSelector = createSelector(baseArtistSelector, baseArtist => get(baseArtist, 'isLoading'))
const errorMsgSelector = createSelector(baseArtistSelector, baseArtist => get(baseArtist, 'errorMsg'))
const selectedEventSelector = createSelector(
    [baseArtistSelector, favoritesEventsSelector],
    (baseArtist, favoritesEventsList) => {
        const selectedEvent = get(baseArtist, 'selectedEvent', null)
        if (selectedEvent) {
            const isFavorite = some(favoritesEventsList, fe => fe.id === selectedEvent.id)
            selectedEvent.isFavorite = isFavorite
            return { ...selectedEvent }
        }
        return null
    })




export {
    artistDetailsSelector,
    artistEventsSelector,
    selectedEventSelector,
    artistsHistorySearchSelector,
    isLoadingSelector,
    errorMsgSelector

}
