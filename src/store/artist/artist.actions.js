import Types from './artist.types'


const searchArtistRequest = (name)=> ({type:Types.SEARCH_ARTIST_REQUEST,payload:name})
const searchArtistSuccess = (artists)=>({type:Types.SEARCH_ARTIST_SUCCESS,payload:artists})
const searchArtistFailure= (error)=>({type:Types.SEARCH_ARTIST_FAILURE,payload:error})
const resetSearch= ()=>({type:Types.RESET_SEARCH})
const selectEventArtist= (data)=>({type:Types.SELECT_EVENT_ARTIST,payload:data})

export {
    searchArtistRequest,
    searchArtistSuccess,
    searchArtistFailure,
    resetSearch,
    selectEventArtist

}