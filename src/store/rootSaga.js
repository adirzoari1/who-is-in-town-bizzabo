import { all, call } from 'redux-saga/effects';

import  artistSaga from './artist/artist.saga'

export default function* rootSaga() {
    yield all([call(artistSaga)]);
}
